package edu.uprm.cse.datastructures.list.problems;

import edu.uprm.cse.datastructures.list.ArrayList;
import edu.uprm.cse.datastructures.list.List;
import static org.junit.Assert.*;

import org.junit.Test;

public class AllTest {
	/* Empty List */
	@Test
	public void FindMinValue1() {
		List<Integer> L = new ArrayList<Integer>();
		
		int min = FindMinValue.findMinValue(L);
		assertTrue("If ArrayList is Empty return", 0 ==min);
	}
	/* Negatives values */
	@Test
	public void FindMinValue2() {
		List<Integer> L = new ArrayList<Integer>();
		L.add(100);
		L.add(80);
		L.add(-17);
		L.add(10);
		L.add(300);
		L.add(12);
		
		int min = FindMinValue.findMinValue(L);
		assertTrue("If ArrayList have negatives", -17 ==min);
	}
	
	/* String List*/
	@Test
	public void Replace1() {
		List<String> L = new ArrayList<String>();
		L.add("Kim");
		L.add("Ned");
		L.add("Ron");
		L.add("Ron");
		L.add("Ron");
		L.add("Jil");
		L.add("Amy");
		L.add("Ron");
		L.add("Ron");
		
		int count = L.replaceAll("Ron", "Tom");	
		
		assertTrue("Total items replaced", 5 == count);	
	}
	/* Empty List*/
	@Test
	public void Replace2() {
		List<String> L = new ArrayList<String>();
		
		int count = L.replaceAll("Ron", "Tom");	
		
		assertTrue("Total items replaced", 0 == count);	
	}
	/* Integer List*/
	@Test
	public void Replace3() {
		List<Integer> L = new ArrayList<Integer>();
		L.add(10);
		L.add(20);
		L.add(10);
		L.add(40);
		L.add(10);
		L.add(80);
		
		int count = L.replaceAll(10, 100);	
		
		assertTrue("Total items replaced", 3 == count);	
	}
	/* String List*/
	@Test
	public void Reverse1() {
		List<String> L = new ArrayList<String>();
		L.add("Kim");
		L.add("Ned");
		L.add("Ron");
		L.add("Jil");
		L.add("Amy");
		L.add("Ron");
		L.reverse();
		assertTrue("The first is end", "Ron" == L.get(5));
		assertTrue("The end is first", "Kim" == L.get(0));
	}
	/* Integer List*/
	@Test
	public void Reverse2() {
		List<Integer> L = new ArrayList<Integer>();
		L.add(20);
		L.add(30);
		L.add(40);
		L.add(50);
		L.add(60);
		L.add(70);
		L.reverse();
		
		assertTrue("The first is end", 70 == L.get(5));
		assertTrue("The end is first", 20 == L.get(0));
	}
	/* Empty List*/
	@Test
	public void TotalCount1() {
		Object[] lists = new Object[3];
		lists[0] = new ArrayList<String>();
		lists[1] = new ArrayList<String>();
		lists[2] = new ArrayList<String>();
		
		List<String> temp = (List<String>) lists[0];
		
		temp = (List<String>) lists[1];

		temp = (List<String>) lists[2];
		
		int count = TotalCount.totalCount("Bob", lists);
	
		assertTrue("Total count of elements", 0 == count);
	}
	/* String List, exist element*/
	@Test
	public void TotalCount2() {
		Object[] lists = new Object[3];
		lists[0] = new ArrayList<String>();
		lists[1] = new ArrayList<String>();
		lists[2] = new ArrayList<String>();
		
		List<String> temp = (List<String>) lists[0];
		temp.add("Bob");
		temp.add("Ron");
		temp.add("Kim");
		temp.add("Bob");

		temp = (List<String>) lists[1];
		temp.add("Bob");
		temp.add("Amy");

		temp = (List<String>) lists[2];
		temp.add("Joe");
		temp.add("Apu");
		temp.add("Jil");
		temp.add("Ned");
		temp.add("Pol");
		
		int count = TotalCount.totalCount("Bob", lists);
		
		assertTrue("Total count of elements", 3 == count);
	}
	/* String List, no exist element*/
	@Test
	public void TotalCount3() {
		Object[] lists = new Object[3];
		lists[0] = new ArrayList<String>();
		lists[1] = new ArrayList<String>();
		lists[2] = new ArrayList<String>();
		
		List<String> temp = (List<String>) lists[0];
		temp.add("Bob");
		temp.add("Ron");
		temp.add("Kim");
		temp.add("Bob");

		temp = (List<String>) lists[1];
		temp.add("Bob");
		temp.add("Amy");

		temp = (List<String>) lists[2];
		temp.add("Joe");
		temp.add("Apu");
		temp.add("Jil");
		temp.add("Ned");
		temp.add("Pol");
		
		int count = TotalCount.totalCount("Lu", lists);
		
		
		assertTrue("Total count of elements", 0 == count);
	}
}